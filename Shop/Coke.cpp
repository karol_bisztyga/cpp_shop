#include "stdafx.h"
#include "Coke.h"

namespace shop
{

	Coke::Coke(std::string expiration_date, float capacity) : expiration_date_(expiration_date), capacity_(capacity)
	{
	}

	std::string Coke::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	float Coke::GetCapacity() const
	{
		return this->capacity_;
	}

	bool Coke::IsCarbonated() const
	{
		return true;
	}

	std::string Coke::GetName() const
	{
		return "coke";
	}

	PRODUCT_TYPE Coke::GetType() const
	{
		return PRODUCT_TYPE::DRINK;
	}

	PRODUCT_CLASS Coke::GetClass() const
	{
		return PRODUCT_CLASS::COKE;
	}

} //namespace shop