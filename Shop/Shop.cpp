#include "stdafx.h"
#include "Shop.h"
#include <chrono>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "CheckoutFactory.h"
#include "CustomerFactory.h"
#include "ProductInterface.h"
#include "ProductInclude.h"
#include "ProductType.h"

namespace shop
{

	const std::string Shop::NAMES_FILE_NAME = "names.txt";

	/**
	*   @brief creates as many Checkouts as needed using CheckoutFactory
	*/
	Shop::Shop(int num_checkouts, long long work_time) : work_time_(work_time)
	{
		this->open_ = false;
		for (int i = 0; i < num_checkouts; ++i)
		{
			this->checkouts_.push_back(CheckoutFactory::GetInstance().GenerateCheckout());
		}
		this->prevent_notify_checkout_ = true;
		this->prevent_notify_queue_ = true;
	}

	float Shop::getPrice(ProductInterface* product)
	{
		if (dynamic_cast<Chocolate*> (product))
		{
			return (1.5f*(dynamic_cast<Chocolate*> (product))->GetWeight()) / .1f;
		}
		if (dynamic_cast<Lollipop*> (product))
		{
			return .5f;
		}
		if (dynamic_cast<Coke*> (product))
		{
			return 4.0f*(dynamic_cast<Coke*> (product))->GetCapacity();
		}
		if (dynamic_cast<Juice*> (product))
		{
			return 3.2f*(dynamic_cast<Juice*> (product))->GetCapacity();
		}
		if (dynamic_cast<Water*> (product))
		{
			return 1.9f*(dynamic_cast<Water*> (product))->GetCapacity();
		}
		if (dynamic_cast<Apple*> (product))
		{
			return 2.7f*(dynamic_cast<Apple*> (product))->GetWeight();
		}
		if (dynamic_cast<Lemon*> (product))
		{
			return 3.2f*(dynamic_cast<Lemon*> (product))->GetWeight();
		}
		if (dynamic_cast<Orange*> (product))
		{
			return 2.9f*(dynamic_cast<Orange*> (product))->GetWeight();
		}
		return 0.0f;
	}

	bool Shop::IsOpen() const
	{
		return this->open_;
	}

	void Shop::Close()
	{
		this->open_ = false;
		Shop::Print("[*] shop closed");
	}

	void Shop::OpenTimer()
	{
		this->open_ = true;
		Shop::Print("[*] shop opened for " + std::to_string(this->work_time_) + " seconds");
		this->open_time_ = this->CurrentTimeMilliseconds();
		Shop::Print("[*] open time: " + std::to_string(this->open_time_));
		std::this_thread::sleep_for(std::chrono::seconds(this->work_time_));
		this->Close();
		Shop::Print("[*] close time: " + std::to_string(this->CurrentTimeMilliseconds()));
	}

	void Shop::CustomerProducer()
	{
		int average_interval = 100;
		while (this->open_)
		{
			std::unique_lock<std::mutex> lock(this->customers_mutex_);
			this->customers_.push(CustomerFactory::GetInstance().GenerateCustomer());
			Shop::Print(
					"[+] customer goes in [" + 
					this->customers_.back().get()->GetName() +
					"], customers in queue: " + 
					std::to_string(this->customers_.size()));
			lock.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(average_interval));
		}
	}

	/**
	*   @brief waits for the free checkout, and once it appears assigns a customer from the queue to it
	*/
	void Shop::QueueCaller()
	{
		while (true)
		{
			this->prevent_notify_queue_ = false;
			std::unique_lock<std::mutex> customers_lock_empty(this->customers_mutex_);
			//waits until a customer appears in the queue
			this->empty_queue_condition_.wait(customers_lock_empty, [this]() {
				if (this->customers_.empty() && !this->open_)
				{
					return true;
				}
				return !this->customers_.empty();
			});
			if (this->customers_.empty() && !this->open_)
			{
				break;
			}
			// prevents QueueChecker from notifying temporarily
			this->prevent_notify_queue_ = true;
			this->prevent_notify_checkout_ = false;
			customers_lock_empty.unlock();
			std::unique_lock<std::mutex> checkouts_lock(this->checkouts_mutex_);
			// waits for a free checkout
			this->free_checkouts_condition_.wait(checkouts_lock, [this]() {
				if (this->customers_.empty() && !this->open_) 
				{
					return true;
				}
				return this->GetFreeCheckout() != nullptr;
			});
			if (this->customers_.empty() && !this->open_)
			{
				break;
			}
			// prevents CheckoutChecker from notifying temporarily
			this->prevent_notify_checkout_ = true;
			std::shared_ptr<Checkout> free_checkout = this->GetFreeCheckout();
			checkouts_lock.unlock();
			// if there is a free checkout and the customer queue is not empty, 
			// locks customers queue, takes the first customer from it 
			// and tries to assign it to the chosen checkout
			std::unique_lock<std::mutex> customers_lock_assign(this->customers_mutex_);
			std::unique_ptr<Customer> customer_assigned = std::move(this->customers_.front());
			std::string name = customer_assigned.get()->GetName();
			customer_assigned = std::move(free_checkout.get()->AssignCustomer(std::move(customer_assigned)));
			this->customers_.pop();
			// if AssignCustomer failed, the customer goes back to the queue
			if (customer_assigned != nullptr)
			{
				this->customers_.push(move(customer_assigned));
			}
			// exits when the customer queue is empty
			if (this->customers_.empty() && !this->open_)
			{
				break;
			}
		}
		// let all the checkout know that the shop is closed
		for (auto it = this->checkouts_.begin(); it < this->checkouts_.end(); ++it)
		{
			(*it).get()->ShopClosed();
		}
	}

	/**
	*   @brief checks for a free checkout in loop and notifies QueueCaller once it appears
	*          exits when the customer queue is empty
	*/
	void Shop::CheckoutChecker()
	{
		while (true)
		{
			while (this->open_ && (this->GetFreeCheckout() == nullptr || this->prevent_notify_checkout_))
			{
				std::this_thread::yield();
			}
			// locks checkouts and checks if there is a free one
			std::unique_lock<std::mutex> checkouts_lock(this->checkouts_mutex_);
			// notifies QueueCaller that free checkout was found
			this->free_checkouts_condition_.notify_all();
			checkouts_lock.unlock();
			// exits when the shop is closed and the queue is empty
			std::unique_lock<std::mutex> customers_lock(this->customers_mutex_);
			if (!this->open_ && this->customers_.empty())
			{
				break;
			}
		}
	}

	/**
	*   @brief checks for any customers in queue in loop and notifies QueueCaller once someone appears
	*          exits when the customer queue is empty
	*/
	void Shop::QueueChecker()
	{
		while (true)
		{
			// locks customers queue and checks if it is empty
			while (this->open_ && (this->customers_.empty() || this->prevent_notify_queue_))
			{
				std::this_thread::yield();
			}
			std::unique_lock<std::mutex> customers_lock(this->customers_mutex_);
			// notifies QueueCaller that the queue is not empty
			this->empty_queue_condition_.notify_all();
			// exits when the shop is closed and the queue is empty
			if (!this->open_ && this->customers_.empty())
			{
				break;
			}
		}
	}

	long long Shop::CurrentTimeMilliseconds() const
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(
				std::chrono::system_clock::now().time_since_epoch()).count();
	}

	/**
	*   @brief responsible for launching all shop's threads and joining them
	*/
	void Shop::operator() ()
	{
		std::thread open_timer_thread(&Shop::OpenTimer, this);
		std::thread customer_producer_thread(&Shop::CustomerProducer, this);
		std::thread queue_caller_thread(&Shop::QueueCaller, this);
		std::thread queue_checker_thread(&Shop::QueueChecker, this);
		std::thread checkout_checker_thread(&Shop::CheckoutChecker, this);


		queue_caller_thread.join();
		checkout_checker_thread.join();
		queue_checker_thread.join();

		// join all threads from checkouts gracefuly
		float total_cash = 0.0f;
		int total_customers = 0;
		int total_products = 0;
		for (auto it = this->checkouts_.begin(); it < this->checkouts_.end(); ++it)
		{
			(*it).get()->JoinThreads();
			total_cash += (*it).get()->GetCash();
			total_customers += (*it).get()->GetCustomers();
			total_products += (*it).get()->GetProducts();
		}

		customer_producer_thread.join();
		open_timer_thread.join();

		Shop::Print("[*] shop exiting, total cash made: " + std::to_string(total_cash) +
				", total customers: " + std::to_string(total_customers) +
				", products sold: " + std::to_string(total_products));
	}

	std::shared_ptr<Checkout> Shop::GetFreeCheckout() const
	{
		for (auto it = this->checkouts_.begin(); it != this->checkouts_.end(); ++it)
		{
			if (!it->get()->IsBusy())
			{
				return *it;
			}
		}
		return nullptr;
	}

	void Shop::Print(std::string info)
	{
		static std::mutex print_mutex_;
		std::unique_lock<std::mutex> lock(print_mutex_);
		std::cout << info << std::endl;
	}

} //namespace shop