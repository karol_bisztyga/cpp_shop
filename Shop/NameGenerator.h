/**
*  @file    NameGenerator.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief tool which reads (from file) a list of available names which would be used to generate customers
*         randomly pick a name from the list, works as singleton, thus reads file only once for a runtime
*
*/

#pragma once

#include <vector>
#include <string>

namespace shop
{

	class NameGenerator
	{
	public:
		NameGenerator(NameGenerator const&) = delete;
		void operator=(NameGenerator const&) = delete;
		static NameGenerator& GetInstance();
		/**
		*   @brief generates random name(one word)
		*
		*   @return name as string
		*/
		std::string GenerateName() const;
		size_t NumNames() const;
	private:
		NameGenerator();
		std::vector<std::string> names;
	};

} //namespace shop

