/**
*  @file    FruitInterface.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief interface for all classes of products which are fruits
*
*/

#pragma once

#include "ProductInterface.h"
#include "Color.h"


namespace shop
{

	class FruitInterface : public ProductInterface
	{
	public:
		/**
		*   @brief determines weight of the fruit, unit - kg
		*
		*   @return weight as float
		*/
		virtual float GetWeight() const = 0;
		virtual COLOR GetColor() const = 0;
	};

} //namespace shop
