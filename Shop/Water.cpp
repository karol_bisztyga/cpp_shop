#include "stdafx.h"
#include "Water.h"

namespace shop
{

	Water::Water(std::string expiration_date, float capacity, bool carbonated) :
			expiration_date_(expiration_date), capacity_(capacity), carbonated_(carbonated)
	{
	}

	std::string Water::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	float Water::GetCapacity() const
	{
		return this->capacity_;
	}

	bool Water::IsCarbonated() const
	{
		return this->carbonated_;
	}

	std::string Water::GetName() const
	{
		std::string name = "water, ";
		if (!this->carbonated_)
		{
			name += "not ";
		}
		return name + "carbonated";
	}

	PRODUCT_TYPE Water::GetType() const
	{
		return PRODUCT_TYPE::DRINK;
	}

	PRODUCT_CLASS Water::GetClass() const
	{
		return PRODUCT_CLASS::WATER;
	}

} //namespace shop