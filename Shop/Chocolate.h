/**
*  @file    Chocolate.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which represents a bar of chocolate
*
*/

#pragma once

#include "CandyInterface.h"

namespace shop
{

	class Chocolate : public CandyInterface
	{
	public:
		Chocolate(std::string, float, bool, int);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetCarbohydrates() const;
		virtual float GetWeight() const;
		virtual bool ContainsChocolate() const;
		bool ContainsNuts() const;
		int GetPieces() const;
	private:
		const std::string expiration_date_;
		const float weight_;
		const bool nuts_;
		const int pieces_;
	};

} //namespace shop