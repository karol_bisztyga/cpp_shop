/**
*  @file    Orange.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which stand for a single orange
*
*/

#pragma once

#include "FruitInterface.h"

namespace shop
{

	class Orange : public FruitInterface
	{
	public:
		Orange(std::string, float);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetWeight() const;
		virtual COLOR GetColor() const;
	private:
		const float weight_;
		const std::string expiration_date_;
	};

} //namespace shop