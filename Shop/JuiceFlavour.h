/**
*  @file    JuiceFlavour.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief enum of basic juice flavours and a simple utility to get their string representation
*
*/

#pragma once

#include <string>

namespace shop
{

	enum class JUICE_FLAVOUR
	{
		APPLE,
		GRAPE,
		APPLE_MINT,
		APPLE_PEACH,
		PEACH,
		MIXED_FRUITS,
		TOMATO,
		STRAWBERRY,
	};

	class JuiceFlavourPrinter
	{
	public:
		/**
		*   @brief converts juice flavour to string
		*
		*   @param juice_flavour - specified juice flavour value
		*   @return representation of a given juice flavour as string. If the flavour is not recognized, returns "unknown"
		*/
		static std::string Print(JUICE_FLAVOUR juice_flavour)
		{
			switch (juice_flavour)
			{
			case JUICE_FLAVOUR::APPLE:
				return "apple";
			case JUICE_FLAVOUR::GRAPE:
				return "grape";
			case JUICE_FLAVOUR::APPLE_MINT:
				return "apple-mint";
			case JUICE_FLAVOUR::APPLE_PEACH:
				return "apple-peach";
			case JUICE_FLAVOUR::PEACH:
				return "peach";
			case JUICE_FLAVOUR::MIXED_FRUITS:
				return "mixed fruits";
			case JUICE_FLAVOUR::TOMATO:
				return "tomato";
			case JUICE_FLAVOUR::STRAWBERRY:
				return "strawberry";
			default:
				return "unknown";
			}
		}
	};

} //namespace shop