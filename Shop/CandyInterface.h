/**
*  @file    CandyInterface.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief interface for all classes of products which are candy
*
*/

#pragma once

#include "ProductInterface.h"

namespace shop
{

	class CandyInterface : public ProductInterface
	{
		/**
		*   @brief describes how much sugar (carbohydrates) ontains this product, unit - g
		*
		*   @return carbohydrates as float
		*/
		virtual float GetCarbohydrates() const = 0;
		/**
		*   @brief weight unit - kg
		*
		*   @return weight as float
		*/
		virtual float GetWeight() const = 0;
		virtual bool ContainsChocolate() const = 0;
	};

} //namespace shop