/**
*  @file    CheckoutFactory.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief singleton factory which produces shop checkouts
*
*/

#pragma once

#include <memory>
#include "Checkout.h"

namespace shop
{

	class CheckoutFactory
	{
	public:
		CheckoutFactory(CheckoutFactory const&) = delete;
		void operator=(CheckoutFactory const&) = delete;
		static CheckoutFactory& GetInstance();
		/**
		*   @brief generates customer with random parameters
		*
		*   @return unique pointer to customer object
		*/
		std::unique_ptr<Checkout> GenerateCheckout();
	private:
		int current_id_;
		CheckoutFactory();
	};

} //namespace shop