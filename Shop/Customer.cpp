#include "stdafx.h"
#include "Customer.h"
#include <random>
#include "Shop.h"

namespace shop
{

	const float Customer::MAX_SPEED = 2.0;

	Customer::Customer(std::string name, bool healthy, float speed, float money) : 
		name_(name), healthy_(healthy), speed_(speed), money_(money)
	{
	}

	const std::string Customer::GetName() const
	{
		return this->name_;
	}

	float Customer::GetSpeed() const
	{
		return this->speed_;
	}

	float Customer::GetMoney() const
	{
		return this->money_;
	}

	bool Customer::IsHealthy() const
	{
		return this->healthy_;
	}

	bool Customer::BuyProduct(ProductInterface* product)
	{
		float price = Shop::getPrice(product);
		if (this->money_ - price  <= 0)
		{
			return false;
		}
		this->shopping_cart_.push(product);
		this->money_ -= price;
		return true;
	}

} //namespace shop