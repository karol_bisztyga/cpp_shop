/**
*  @file    ProductType.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/04/01
*  @version 1.0
*
*  @brief defines enums for product types and classes nad utility to get their string representation
*
*/

#pragma once

#include <string>

namespace shop
{

	enum class PRODUCT_TYPE
	{
		DRINK,
		CANDY,
		FRUIT,
	};

	enum class PRODUCT_CLASS
	{
		CHOCOLATE,
		LOLLIPOP,
		COKE,
		JUICE,
		WATER,
		APPLE,
		LEMON,
		ORANGE,
	};

	/**
	*   @brief prints both type and class
	*/
	class ProductTypePrinter
	{
	public:
		/**
		*   @brief converts product type to string
		*
		*   @param product_type - specified product type
		*   @return representation of a given product type as string. If the type is not recognized, returns "unknown"
		*/
		static std::string Print(PRODUCT_TYPE product_type)
		{
			switch (product_type)
			{
			case PRODUCT_TYPE::CANDY:
				return "candy";
			case PRODUCT_TYPE::DRINK:
				return "drink";
			case PRODUCT_TYPE::FRUIT:
				return "fruit";
			default:
				return "unknown";
			}
		}
		/**
		*   @brief converts product class to string
		*
		*   @param product_class - specified product class
		*   @return representation of a given product class as string. If the class is not recognized, returns "unknown"
		*/
		static std::string Print(PRODUCT_CLASS product_class)
		{
			switch (product_class)
			{
			case PRODUCT_CLASS::CHOCOLATE:
				return "chocolate";
			case PRODUCT_CLASS::LOLLIPOP:
				return "lollipop";
			case PRODUCT_CLASS::COKE:
				return "coke";
			case PRODUCT_CLASS::JUICE:
				return "juice";
			case PRODUCT_CLASS::WATER:
				return "water";
			case PRODUCT_CLASS::APPLE:
				return "apple";
			case PRODUCT_CLASS::LEMON:
				return "lemon";
			case PRODUCT_CLASS::ORANGE:
				return "orange";
			default:
				return "unknown";
			}
		}
	};

} //namespace shop