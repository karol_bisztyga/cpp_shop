/**
*  @file    Juice.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which represents a bottle of fruit juice
*
*/

#pragma once

#include "DrinkInterface.h"
#include "JuiceFlavour.h"

namespace shop
{

	class Juice : public DrinkInterface
	{
	public:
		Juice(std::string, float, JUICE_FLAVOUR);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetCapacity() const;
		virtual bool IsCarbonated() const;
		JUICE_FLAVOUR GetFlavour() const;
	private:
		const float capacity_;
		const std::string expiration_date_;
		const JUICE_FLAVOUR flavour_;
	};

} //namespace shop