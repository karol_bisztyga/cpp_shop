#include "stdafx.h"
#include "Lemon.h"

namespace shop
{

	Lemon::Lemon(std::string expiration_date, float weight) : expiration_date_(expiration_date), weight_(weight)
	{
	}

	std::string Lemon::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	std::string Lemon::GetName() const
	{
		return "lemon";
	}

	PRODUCT_TYPE Lemon::GetType() const
	{
		return PRODUCT_TYPE::FRUIT;
	}

	PRODUCT_CLASS Lemon::GetClass() const
	{
		return PRODUCT_CLASS::LEMON;
	}

	float Lemon::GetWeight() const
	{
		return this->weight_;
	}

	COLOR Lemon::GetColor() const
	{
		return COLOR::YELLOW;
	}

} //namespace shop