/**
*  @file    CustomerFactory.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief singleton factory which produces customers with random parameters
*
*/

#pragma once

#include <memory>
#include "Customer.h"

namespace shop
{

	class CustomerFactory
	{
	public:
		CustomerFactory(CustomerFactory const&) = delete;
		void operator=(CustomerFactory const&) = delete;
		static CustomerFactory& GetInstance();
		/**
		*   @brief generates customer with random parameters
		*
		*   @return unique pointer to customer object
		*/
		std::unique_ptr<Customer> GenerateCustomer() const;
	private:
		CustomerFactory();
	};

} //namespace shop


