/**
*  @file    DrinkInterface.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief interface for all classes of products which are drinks
*
*/

#pragma once

#include "ProductInterface.h"

namespace shop
{

	class DrinkInterface : public ProductInterface
	{
	public:
		/**
		*   @brief determines the capacity of this drink bottle, unit - l
		*
		*   @return capacity as float
		*/
		virtual float GetCapacity() const = 0;
		virtual bool IsCarbonated() const = 0;
	};

} //namespace shop