#include "stdafx.h"
#include "Chocolate.h"

namespace shop
{

	Chocolate::Chocolate(std::string expiration_date, float weight, bool nuts, int pieces) :
			expiration_date_(expiration_date), weight_(weight), nuts_(nuts), pieces_(pieces)
	{
	}

	std::string Chocolate::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	std::string Chocolate::GetName() const
	{
		std::string name = "chocolate";
		if (this->nuts_)
		{
			name += " with nuts";
		}
		return name + ", " + std::to_string(this->pieces_) + " pieces";
	}

	PRODUCT_TYPE Chocolate::GetType() const
	{
		return PRODUCT_TYPE::CANDY;
	}

	PRODUCT_CLASS Chocolate::GetClass() const
	{
		return PRODUCT_CLASS::CHOCOLATE;
	}

	float Chocolate::GetCarbohydrates() const
	{
		float nut_modifier = (this->nuts_) ? .9f : 1.0f ;
		return this->weight_ * 41.0f / 100.0f * nut_modifier;
	}

	float Chocolate::GetWeight() const
	{
		return this->weight_;
	}

	bool Chocolate::ContainsChocolate() const
	{
		return true;
	}

	bool Chocolate::ContainsNuts() const
	{
		return this->nuts_;
	}

	int Chocolate::GetPieces() const
	{
		return this->pieces_;
	}


} //namespace shop