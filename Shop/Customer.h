/**
*  @file    Customer.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief representation of a single customer who would pick products, stay in queue, 
*         approach a check-out and make payment
*
*/

#pragma once

#include <string>
#include <stack>
#include "ProductInterface.h"

namespace shop
{

	class Customer
	{
	public:
		static const float MAX_SPEED;
		Customer(std::string, bool, float, float);
		float GetSpeed() const;
		float GetMoney() const;
		bool IsHealthy() const;
		const std::string GetName() const;
		/**
		*   @brief if the customer affords the product, puts it in the shopping cart and
		*          pulls specified amount of meoney from the customer
		*
		*   @return true if transaction succeeded, false otherwise
		*/
		bool BuyProduct(ProductInterface*);
	private:
		const std::string name_;
		// if true, customer is willing to buy more healthy products
		// if false they do not care
		const bool healthy_;
		// tells how fast the customer is at packing/repacking products
		// affects speed while buying
		// values range: 0.0f(slowest) - 2.0f(fastest)
		float speed_;
		// how much money is the customer willing to spend on shopping
		// or how much money left they have
		float money_;
		// represents the shopping cart
		std::stack<ProductInterface*> shopping_cart_;
	};

} //namespace shop