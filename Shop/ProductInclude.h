/**
*  @file    ProductInclude.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief include file which contains all necessary headers to operate on any product defined in project
*
*/

#pragma once

#include "Chocolate.h"
#include "Lollipop.h"
#include "Coke.h"
#include "Juice.h"
#include "Water.h"
#include "Apple.h"
#include "Lemon.h"
#include "Orange.h"