/**
*  @file    Checkout.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief representation of a single shop check-out which is responsible for processing all picked products and
*         collecting money from customers
*
*/

#pragma once

#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "Customer.h"

namespace shop
{

	class Checkout
	{
	public:
		/**
		*   @param id as int
		*/
		Checkout(int);
		const int GetId() const;
		/**
		*   @brief tells whether this checkout is occupied by a customer at the moment or not
		*
		*   @return false if current_customer is equal to nullptr, otherwise true
		*/
		bool IsBusy() const;
		/**
		*   @brief assigns a customer to this checkout if it is currently free
		*
		*   @return true if assigning was successful or false if was not
		*/
		std::unique_ptr<Customer> AssignCustomer(std::unique_ptr<Customer>);
		/**
		*   @brief lets the checkout know that the customers queue is empty and the shop is closed
		*
		*   @return void
		*/
		void ShopClosed();
		/**
		*   @brief joining all the threads
		*
		*   @return void
		*/
		void JoinThreads();
		/**
		*   @brief tells how much money has this checkout earned
		*
		*   @return total cash as float
		*/
		float GetCash() const;
		/**
		*   @brief tells how many customers have been handled by this checkout
		*
		*   @return total customers as int
		*/
		int GetCustomers() const;
		/**
		*   @brief tells how many products have been sold
		*
		*   @return total products as int
		*/
		int GetProducts() const;
	private:
		// id of the checkout, should be unique for every one of them in each shop
		const int id_;
		// pointer to a customer who is being served at the moment
		std::unique_ptr<Customer> current_customer_;
		// stores sum of money collected from customers
		float cash_;
		// checkout is active until the shop is open and the customers queue is not empty
		std::atomic<bool> active_;
		// used to improve workflow of CheckCustomer
		std::atomic<bool> prevent_notify_check_customer_;
		// mutex used just to make sure the checkout handles one customer at a time
		std::mutex customer_mutex_;
		// tells whether a customer is present at the checkout
		std::condition_variable customer_condition_;
		// tells how many customers have been handled by this checkout
		int customers_count_;
		// tells how many products have been sold by this checkout
		int products_count_;
		// vector of pointers to threads, used later to join them gracefuly
		std::queue<std::unique_ptr<std::thread>> threads;
		/**
		*   @brief waits for the customer, handles them, waits required amount of time for checking products
		*          and receives the payment
		*
		*   @return void
		*/
		void HandleCustomer();
		/**
		*   @brief keep checking if a customer came, if so, notifies HandleCustomer
		*
		*   @return void
		*/
		void CheckCustomer();

	};

} //namespace shop