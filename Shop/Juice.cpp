#include "stdafx.h"
#include "Juice.h"
#include "JuiceFlavour.h"

namespace shop
{

	Juice::Juice(std::string expiration_date, float capacity, JUICE_FLAVOUR flavour) :
			expiration_date_(expiration_date), capacity_(capacity), flavour_(flavour)
	{
	}

	std::string Juice::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	float Juice::GetCapacity() const
	{
		return this->capacity_;
	}

	bool Juice::IsCarbonated() const
	{
		return false;
	}

	std::string Juice::GetName() const
	{
		return JuiceFlavourPrinter::Print(this->flavour_) + " juice";
	}

	PRODUCT_TYPE Juice::GetType() const
	{
		return PRODUCT_TYPE::DRINK;
	}

	PRODUCT_CLASS Juice::GetClass() const
	{
		return PRODUCT_CLASS::JUICE;
	}

	JUICE_FLAVOUR Juice::GetFlavour() const
	{
		return this->flavour_;
	}


} //namespace shop