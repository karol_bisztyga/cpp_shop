#include "stdafx.h"
#include "ProductFactory.h"
#include <memory>
#include <random>
#include <ctime>
#include <algorithm>
#include "ProductInclude.h"

namespace shop
{

	ProductFactory::ProductFactory()
	{
	}

	ProductFactory& ProductFactory::GetInstance()
	{
		static ProductFactory instance;
		return instance;
	}

	/**
	*   @brief the parameters and their ranges are different for every type of product
	*
	*   @return product with random parameters
	*/
	std::unique_ptr<ProductInterface> ProductFactory::GenerateProduct() const
	{
		float size = 0.0f;
		int random_product_index = rand() % 8;
		switch (random_product_index)
		{
		case 0:
			size = 0.1f * static_cast<float>(rand() % 3 + 1);
			return std::make_unique<Chocolate>(
				this->GenerateExpirationDate(PRODUCT_TYPE::CANDY), size, (rand()%2), (rand()%3+2)*(rand()%5+4));
		case 1:
			return std::make_unique<Lollipop>(this->GenerateExpirationDate(PRODUCT_TYPE::CANDY));
		case 2:
		case 3:
		case 4:
			switch (rand() % 4)
			{
			case 0:
				size = .5f;
				break;
			case 1:
				size = 1.0f;
				break;
			case 2:
				size = 1.5f;
				break;
			case 3:
				size = 2.0f;
				break;
			}
			if (random_product_index == 2)
			{
				return std::make_unique<Coke>(this->GenerateExpirationDate(PRODUCT_TYPE::DRINK), size);
			}
			else if (random_product_index == 3)
			{
				return std::make_unique<Juice>(
					this->GenerateExpirationDate(PRODUCT_TYPE::DRINK), size, JUICE_FLAVOUR::APPLE_PEACH);
			}
			else if (random_product_index == 4)
			{
				return std::make_unique<Water>(this->GenerateExpirationDate(PRODUCT_TYPE::DRINK), size, (rand()%2));
			}
		case 5:
			size = .1f * static_cast<float>(rand()%4+2);
			COLOR color;
			switch (rand()%3)
			{
			case 0:
				color = COLOR::RED;
				break;
			case 1:
				color = COLOR::YELLOW;
				break;
			case 2:
				color = COLOR::GREEN;
				break;
			}
			return std::make_unique<Apple>(this->GenerateExpirationDate(PRODUCT_TYPE::FRUIT), size, color);
		case 6:
			size = .1f * static_cast<float>(rand() % 2 + 1);
			return std::make_unique<Lemon>(this->GenerateExpirationDate(PRODUCT_TYPE::FRUIT), size);
		case 7:
			size = .1f * static_cast<float>(rand() % 5 + 3);
			return std::make_unique<Orange>(this->GenerateExpirationDate(PRODUCT_TYPE::FRUIT), size);
		default:
			return nullptr;
		}
	}

	/**
	*   @brief expiration date is generated randomly, ranges depend on product type
	*          date format is "YYYY-MM-DD"
	*
	*   @return date as string
	*/
	std::string ProductFactory::GenerateExpirationDate(PRODUCT_TYPE product_type) const
	{
		struct tm newtime;
		time_t t = time(0);
		errno_t err = localtime_s(&newtime, &t);
		if (err)
		{
			return "unknown-err";
		}
		int year = newtime.tm_year + 1900;
		int month = newtime.tm_mon + 1;
		int day = newtime.tm_mday;
		switch (product_type)
		{
		case shop::PRODUCT_TYPE::DRINK:
			year += rand() % 2 + 1;
			month = std::max((month + rand()%5) % 12, 1);
			day = std::max((day + rand()%15) % 30, 1);
			break;
		case shop::PRODUCT_TYPE::CANDY:
			year += rand() % 3 + 1;
			month = std::max((month + rand() % 2) % 12, 1);
			day = std::max((day + rand() % 10) % 30, 1);
			break;
		case shop::PRODUCT_TYPE::FRUIT:
			month = std::max((month + rand() % 7) % 12, 1);
			day = std::max((day + rand() % 5) % 30, 1);
			break;
		default:
			return "unknown";
		}
		std::string month_text = (month>=10) ? std::to_string(month) : "0" + std::to_string(month);
		std::string day_text = (day>=10) ? std::to_string(day) : "0" + std::to_string(day);
		return std::to_string(year) + "-" + month_text + "-" + day_text;
	}

}