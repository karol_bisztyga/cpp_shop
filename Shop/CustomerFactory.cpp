#include "stdafx.h"
#include "CustomerFactory.h"
#include <memory>
#include "NameGenerator.h"

namespace shop
{

	CustomerFactory::CustomerFactory()
	{
	}

	CustomerFactory& CustomerFactory::GetInstance()
	{
		static CustomerFactory instance;
		return instance;
	}

	/**
	*   @brief ranges of randomly generated parameters:
	*	    -speed: .5f - 1.5f
	*	    -money: 10.0f - 30.0f
	*	    -name: taken from NameGenerator
	*
	*   @return customer with random parameters
	*/
	std::unique_ptr<Customer> CustomerFactory::GenerateCustomer() const
	{
		float speed = static_cast<float>(rand() % 11 + 5)*.1f;
		float money = static_cast<float>(rand() % 31 + 10)*1.0f;
		const std::string name = NameGenerator::GetInstance().GenerateName();
		return std::make_unique<Customer>(name, rand()%2, speed, money);
	}


} //namespace shop