/**
*  @file    ProductFactory.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief enum of product main types and the singleton factory which produces products with random parameters
*
*/

#pragma once

#include <memory>
#include "ProductInterface.h"
#include "ProductType.h"

namespace shop
{

	class ProductFactory
	{
	public:
		ProductFactory(ProductFactory const&) = delete;
		void operator=(ProductFactory const&) = delete;
		static ProductFactory& GetInstance();
		/**
		*   @brief generates product implementation with random parameters
		*
		*   @return unique pointer to product object
		*/
		std::unique_ptr<ProductInterface> GenerateProduct() const;
	private:
		ProductFactory();
		std::string GenerateExpirationDate(PRODUCT_TYPE) const;

	};

} //namespace shop