#include "stdafx.h"
#include "Lollipop.h"

namespace shop
{

	Lollipop::Lollipop(std::string expiration_date) : expiration_date_(expiration_date)
	{
	}

	std::string Lollipop::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	std::string Lollipop::GetName() const
	{
		return "lollipop";
	}

	PRODUCT_TYPE Lollipop::GetType() const
	{
		return PRODUCT_TYPE::CANDY;
	}

	PRODUCT_CLASS Lollipop::GetClass() const
	{
		return PRODUCT_CLASS::LOLLIPOP;
	}

	float Lollipop::GetCarbohydrates() const
	{
		return 7.5f;
	}

	float Lollipop::GetWeight() const
	{
		return 12.5f;
	}

	bool Lollipop::ContainsChocolate() const
	{
		return false;
	}


} //namespace shop