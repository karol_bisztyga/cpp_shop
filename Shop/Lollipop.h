/**
*  @file    Lollipop.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which represents lollipop
*
*/

#pragma once

#include "CandyInterface.h"

namespace shop
{

	class Lollipop : public CandyInterface
	{
	public:
		Lollipop(std::string);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetCarbohydrates() const;
		virtual float GetWeight() const;
		virtual bool ContainsChocolate() const;
	private:
		const std::string expiration_date_;
	};

} //namespace shop