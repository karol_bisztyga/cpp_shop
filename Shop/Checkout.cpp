#include "stdafx.h"
#include "Checkout.h"
#include <thread>
#include <mutex>
#include <memory>
#include "Shop.h"
#include "ProductFactory.h"
#include "Customer.h"

namespace shop
{

	Checkout::Checkout(int id) : id_(id), cash_(0), customers_count_(0)
	{
		this->prevent_notify_check_customer_ = false;
		this->current_customer_ = nullptr;
		this->active_ = true;

		this->threads.push(std::make_unique<std::thread>(&Checkout::HandleCustomer, this));
		this->threads.push(std::make_unique<std::thread>(&Checkout::CheckCustomer, this));
	}

	const int Checkout::GetId() const
	{
		return this->id_;
	}

	bool Checkout::IsBusy() const
	{
		return (this->current_customer_ != nullptr);
	}

	std::unique_ptr<Customer> Checkout::AssignCustomer(std::unique_ptr<Customer> customer)
	{
		if (this->current_customer_ != nullptr)
		{
			return std::move(customer);
		}
		this->current_customer_ = std::move(customer);
		return std::move(nullptr);
	}

	void Checkout::ShopClosed()
	{
		this->active_ = false;
	}

	void Checkout::JoinThreads()
	{
		while (!this->threads.empty())
		{
			(this->threads.front()).get()->join();
			this->threads.pop();
		}
	}

	float Checkout::GetCash() const
	{
		return this->cash_;
	}

	int Checkout::GetCustomers() const
	{
		return this->customers_count_;
	}

	int  Checkout::GetProducts() const
	{
		return this->products_count_;
	}

	void Checkout::HandleCustomer()
	{
		while (this->active_)
		{
			this->prevent_notify_check_customer_ = false;
			std::unique_lock<std::mutex> customer_lock(this->customer_mutex_);
			this->customer_condition_.wait(customer_lock, [this]() {
				if (!this->active_)
				{
					return true;
				}
				return this->current_customer_ != nullptr;
			});
			this->prevent_notify_check_customer_ = true;
			if (!this->active_)
			{
				break;
			}
			// handles the customer
			// if the customer does not like the random product, they skip it
			// after 3 skips they walk away
			int skips_left = 3;
			// loops until customer has enough money to buy more products
			while (true)
			{
				auto random_product = ProductFactory::GetInstance().GenerateProduct();
				float price = Shop::getPrice(random_product.get());
				// interval here depends on customer's speed, it simulates making decision process
				std::this_thread::sleep_for(std::chrono::milliseconds(100)*
						(Customer::MAX_SPEED-this->current_customer_.get()->GetSpeed()));
				// if customer cannot affort this product or is not healthy for them(and they do care), they skip it
				// after 3 skips they leave the shop
				if (
					(this->current_customer_.get()->IsHealthy() &&
						(random_product.get()->GetType() == PRODUCT_TYPE::CANDY ||
						random_product.get()->GetClass() == PRODUCT_CLASS::COKE)) ||
					!this->current_customer_.get()->BuyProduct(random_product.get()))
				{
					--skips_left;
					if (skips_left <= 0)
					{
						break;
					}
					continue;
				}
				this->cash_ += price;
				++this->products_count_;
			}
			//customer has been handled and is released, going home etc.
			++this->customers_count_;
			Shop::Print("[checkout " + std::to_string(this->id_) + "] customer "+ this->current_customer_->GetName()
				+" goes home with money: " + std::to_string(this->current_customer_->GetMoney()));
			this->current_customer_.reset();
		}
	}

	void Checkout::CheckCustomer()
	{
		while (this->active_)
		{
			while (this->active_ && (this->current_customer_ == nullptr || this->prevent_notify_check_customer_))
			{
				std::this_thread::yield();
			}
			std::unique_lock<std::mutex> customer_lock(this->customer_mutex_);
			this->customer_condition_.notify_all();
		}
	}

} //namespace shop