#include "stdafx.h"
#include "Apple.h"

namespace shop
{

	Apple::Apple(std::string expiration_date, float weight, COLOR color) :
			expiration_date_(expiration_date), weight_(weight), color_(color)
	{
	}

	std::string Apple::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	std::string Apple::GetName() const
	{
		return ColorPrinter::Print(this->color_) + " apple";
	}

	PRODUCT_TYPE Apple::GetType() const
	{
		return PRODUCT_TYPE::FRUIT;
	}

	PRODUCT_CLASS Apple::GetClass() const
	{
		return PRODUCT_CLASS::APPLE;
	}

	float Apple::GetWeight() const
	{
		return this->weight_;
	}

	COLOR Apple::GetColor() const
	{
		return this->color_;
	}


} //namespace shop