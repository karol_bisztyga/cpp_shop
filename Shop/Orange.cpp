#include "stdafx.h"
#include "Orange.h"

namespace shop
{

	Orange::Orange(std::string expiration_date, float weight) : expiration_date_(expiration_date), weight_(weight)
	{
	}

	std::string Orange::GetExpirationDate() const
	{
		return this->expiration_date_;
	}

	std::string Orange::GetName() const
	{
		return "orange";
	}

	PRODUCT_TYPE Orange::GetType() const
	{
		return PRODUCT_TYPE::FRUIT;
	}

	PRODUCT_CLASS Orange::GetClass() const
	{
		return PRODUCT_CLASS::ORANGE;
	}

	float Orange::GetWeight() const
	{
		return this->weight_;
	}

	COLOR Orange::GetColor() const
	{
		return COLOR::ORANGE;
	}


} //namespace shop