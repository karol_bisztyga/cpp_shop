/**
*  @file    Color.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief enum of basic colors and a simple utility to get their string representation
*
*/

#pragma once

#include <ostream>
#include <string>

namespace shop
{

	enum class COLOR
	{
		RED,
		GREEN,
		BLUE,
		YELLOW,
		BLACK,
		WHITE,
		PURPLE,
		PINK,
		ORANGE,
	};

	class ColorPrinter
	{
	public:
		/**
		*   @brief converts color to string
		*
		*   @param juice_flavour - specified color value
		*   @return representation of a given color as string. If the color is not recognized, returns "unknown"
		*/
		static std::string Print(COLOR color)
		{
			switch (color)
			{
			case COLOR::RED:
				return "red";
			case COLOR::GREEN:
				return "green";
			case COLOR::BLUE:
				return "blue";
			case COLOR::YELLOW:
				return "yellow";
			case COLOR::BLACK:
				return "black";
			case COLOR::WHITE:
				return "white";
			case COLOR::PURPLE:
				return "purple";
			case COLOR::PINK:
				return "pink";
			case COLOR::ORANGE:
				return "orange";
			default:
				return "unknown";
			}
		}
	};

} //namespace shop