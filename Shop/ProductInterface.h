/**
*  @file    ProductInterface.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief interface for all classes of products
*
*/

#pragma once

#include <string>
#include "ProductType.h"

namespace shop
{

	class ProductInterface
	{
	public:
		/**
		*   @brief date format - "YYYY-MM-DD"
		*
		*   @return date as string
		*/
		virtual std::string GetExpirationDate() const = 0;
		/**
		*   @brief representation of this product
		*
		*   @return name as string
		*/
		virtual std::string GetName() const = 0;
		/**
		*   @brief determines type of the product
		*
		*   @return type as PRODUCT_TYPE enum
		*/
		virtual PRODUCT_TYPE GetType() const = 0;
		/**
		*   @brief determines class of the product
		*
		*   @return type as PRODUCT_CLASS enum
		*/
		virtual PRODUCT_CLASS GetClass() const = 0;
	};

} //namespace shop