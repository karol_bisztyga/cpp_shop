/**
*  @file    Apple.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which represents an apple
*
*/

#pragma once

#include "FruitInterface.h"

namespace shop
{

	class Apple : public FruitInterface
	{
	public:
		Apple(std::string, float, COLOR);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetWeight() const;
		virtual COLOR GetColor() const;
	private:
		const float weight_;
		const std::string expiration_date_;
		const COLOR color_;
	};

} //namespace shop