#include "stdafx.h"
#include "NameGenerator.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <random>
#include "Shop.h"

namespace shop {

	NameGenerator& NameGenerator::GetInstance()
	{
		static NameGenerator instance;
		return instance;
	}

	std::string NameGenerator::GenerateName() const
	{
		return this->names.at(rand() % this->names.size());
	}

	size_t NameGenerator::NumNames() const
	{
		return this->names.size();
	}

	NameGenerator::NameGenerator()
	{
		std::ifstream file(Shop::NAMES_FILE_NAME);
		std::string name;
		while (file >> name)
		{
			this->names.push_back(name);
		}
	}

} //namespace shop