#include "stdafx.h"
#include "CheckoutFactory.h"


namespace shop
{

	CheckoutFactory::CheckoutFactory() : current_id_(0)
	{
	}

	CheckoutFactory& CheckoutFactory::GetInstance()
	{
		static CheckoutFactory instance;
		return instance;
	}

	std::unique_ptr<Checkout> CheckoutFactory::GenerateCheckout()
	{
		return std::make_unique<Checkout>(++this->current_id_);
	}

} //namespace shop