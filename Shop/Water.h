/**
*  @file    Water.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief class which stands for a bottle of water
*
*/

#pragma once

#include "DrinkInterface.h"

namespace shop
{

	class Water : public DrinkInterface
	{
	public:
		Water(std::string, float, bool);
		virtual std::string GetExpirationDate() const;
		virtual std::string GetName() const;
		PRODUCT_TYPE GetType() const;
		PRODUCT_CLASS GetClass() const;
		virtual float GetCapacity() const;
		virtual bool IsCarbonated() const;
	private:
		const std::string expiration_date_;
		const float capacity_;
		const bool carbonated_;
	};

} //namespace shop