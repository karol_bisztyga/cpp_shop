/**
*  @file    Shop.h
*  @author  Karol Bisztyga (karolbisztyga@gmail.com)
*  @date    2018/03/29
*  @version 1.0
*
*  @brief representation of the entire shop. This class would store every other object in the project
*
*/

#pragma once

#include <memory>
#include <vector>
#include <queue>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "ProductInterface.h"
#include "Checkout.h"
#include "Customer.h"

namespace shop
{

	class Shop
	{
	public:
		// file name for NameGenerator to read names from
		static const std::string NAMES_FILE_NAME;
		/**
		*   @param int - number of checkouts in this shop
		*   @param long long - work time in seconds. Defines how long shop will receive new customers
		*/
		Shop(int, long long);
		bool IsOpen() const;
		/**
		*   @brief it should be used to start the shop
		*
		*   @return void
		*/
		void operator() ();
		/**
		*   @brief thread safe print string
		*
		*   @return void
		*/
		static void Print(std::string);
		static float getPrice(ProductInterface*);
	private:
		// how long the shop is open, in seconds
		const long long work_time_;
		// time when shop has opened, in milliseconds since 1970
		long long open_time_;
		// tells whether the shop is open which means it is able to receive new customers
		std::atomic<bool> open_;
		// flag used to improve workflow of empty_queue_condition_
		std::atomic<bool> prevent_notify_queue_;
		// flag used to improve workflow of free_checkouts_condition_
		std::atomic<bool> prevent_notify_checkout_;
		// used to lock operations on checkouts
		std::mutex checkouts_mutex_;
		// used to lock operations on customers queue
		std::mutex customers_mutex_;
		// condition variable for checking if there are any free checkouts
		std::condition_variable free_checkouts_condition_;
		// condition variable for checking if there are any customers in queue
		std::condition_variable empty_queue_condition_;
		// vector of pointers to checkout objects
		std::vector<std::shared_ptr<Checkout>> checkouts_;
		// queue of customers waiting to be handled
		std::queue<std::unique_ptr<Customer>> customers_;
		/**
		*   @brief closes the shop, sets the states properly
		*
		*   @returns void
		*/
		void Close();
		/**
		*   @brief sets states for open shop, runs timer for period specified in work_time_ and calls close()
		*
		*   @returns void
		*/
		void OpenTimer();
		/**
		*   @brief produces new customers randomly with random intervals 
		*          and puts them in the queue as long as the shop is open
		*
		*   @returns void
		*/
		void CustomerProducer();
		/**
		*   @brief calls a single customer from the queue and assigns it to the first free checkout
		*          if no checkouts are free, waits for one
		*
		*   @returns void
		*/
		void QueueCaller();
		/**
		*   @brief checks whether there are any free checkouts. If so, notifies QueueCaller
		*
		*   @returns void
		*/
		void CheckoutChecker();
		/**
		*   @brief checks whether there are any free checkouts. If so, notifies QueueCaller
		*
		*   @returns void
		*/
		void QueueChecker();
		/**
		*   @brief gets current time in milliseconds since 1970
		*
		*   @returns milliseconds as long long
		*/
		long long CurrentTimeMilliseconds() const;
		/**
		*   @brief gets first found checkout with no customer assigned
		*
		*   @returns pointer to the Checkout object
		*/
		std::shared_ptr<Checkout> GetFreeCheckout() const;
	};

} //namespace shop