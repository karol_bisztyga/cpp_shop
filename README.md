# README #

### What is this repository for? ###

* c++ application simulating shop with checkouts, products, customers queue etc., playing with multithreading, synchronizing etc.
* version 1.0

### How do I get set up? ###

* This application is built in Visual Studio 15.6.4. 
* Just clone the repository, load the solution (.sln) file and You should be good to go

### Contact ###

* karolbisztyga@gmail.com